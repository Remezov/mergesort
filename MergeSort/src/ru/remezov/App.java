package ru.remezov;

public class App {
    public static void main(String[] args) {
//        Чтобы запустить сортировку, нужно ввести параметр, равный длине массива.
        start();
    }

    private static void start(int count) {
        Merge merge = new Merge();
        int[] result = merge.start(count);
        System.out.println();
        for (int i = 0; i < result.length ; i++) {

            System.out.print(result[i] + " ");
        }

    }
}
