package ru.remezov;

public class Merge {

    public Merge() {
    }

    public int[] start(int count) {
        int[] array = new int[count];
        for (int i = 0; i < count; i++) {
            array[i] = (int) (Math.random() * (count + 1));
            System.out.print(array[i] + " ");
        }
        return rec(array);
    }

    private int[] rec(int[] array) {
        if (array == null) {
            return null;
        }
        if (array.length < 2) {
            return array;
        }
        int [] arrayA = new int[array.length / 2];
        System.arraycopy(array, 0, arrayA, 0, array.length/2);
        int [] arrayB = new int[array.length - array.length / 2];
        System.arraycopy(array, array.length/2, arrayB, 0, array.length - array.length / 2);
        arrayA = rec(arrayA);
        arrayB = rec(arrayB);
        return merge(arrayA, arrayB);

    }

    private int[] merge(int[] arrayA, int[] arrayB) {
        int[] array = new int[arrayA.length + arrayB.length];
        int positionA = 0, positionB = 0;

        for (int i = 0; i < array.length; i++) {
            if (positionA == arrayA.length){
                array[i] = arrayB[i - positionA];
                positionB++;
            } else if (positionB == arrayB.length) {
                array[i] = arrayA[i - positionB];
                positionA++;
            } else if (arrayA[i - positionB] < arrayB[i - positionA]) {
                array[i] = arrayA[i - positionB];
                positionA++;
            } else {
                array[i] = arrayB[i - positionA];
                positionB++;
            }
        }
        return array;
    }
}
